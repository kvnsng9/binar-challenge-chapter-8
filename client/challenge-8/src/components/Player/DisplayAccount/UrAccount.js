import React, { Component, Fragment } from 'react'
import './UrAccount.css'

export default class UrAccount extends Component {
  render() {
    const userData = JSON.parse(this.props.match.params.data)
    return (
      <Fragment>
        <h1>Your Account</h1>
        <table>
          <tbody key="tbody">
          <tr>
            <td>Username</td>
            <td>:</td>
            <td>{userData.username}</td>
          </tr>
          <tr>
            <td>Email</td>
            <td>:</td>
            <td>{userData.email}</td>
          </tr>
          <tr>
            <td>Password</td>
            <td>:</td>
            <td>{userData.password}</td>
          </tr>
          <tr>
            <td>Exp</td>
            <td>:</td>
            <td>{userData.exp}</td>
          </tr>
          <tr>
            <td>Lvl</td>
            <td>:</td>
            <td>{userData.lvl}</td>
          </tr>
          </tbody>
        </table>
      </Fragment>
    )
  }
}
