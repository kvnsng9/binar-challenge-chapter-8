import React, { Component, Fragment } from 'react'
import './EditAccount.css'
import Button from '../../Button'

export default class EditAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      exp: 0,
      lvl: 0
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    this.setState({
      ...this.state,
      [event.target.name]: value
  });
  }

  handleSubmit(event) {
    alert('username: ' + this.state.username + '\nemail: ' + this.state.email + '\npassword: ' + this.state.password + '\nexp: ' + this.state.exp);
    event.preventDefault();
  }

  render() {
    return (
      <Fragment>
        <h1>Edit Account</h1>
        <form onSubmit={this.handleSubmit}>
            <input type="text" value={this.state.username} name="username" onChange={this.handleChange} placeholder="nana" />
            <br/>
            <input type="text" value={this.state.email} name="email" onChange={this.handleChange} placeholder="aduduh@gmail.com" />
          <br/>
            <input type="password" value={this.state.password} name="password" onChange={this.handleChange} placeholder="*********" />
          <br/>
            <input type="text" value={this.state.exp} name="exp" onChange={this.handleChange} placeholder="0" />
          <br/>
            <input type="text" value={this.state.lvl} name="lvl" onChange={this.handleChange} placeholder="2" />
          <br/>
          <Button
            className="btns"
            type="submit"
            buttonStyle="btn--outline"
            buttonSize="btn--large"
          >
            Submit
          </Button>
        </form>
      </Fragment>
    )
  }
}
