import React, { Component, Fragment } from 'react'
import './CreatePlayer.css'
import Button from '../../Button'

export default class CreatePlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      exp: 0,
      lvl: 0
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    this.setState({
      ...this.state,
      [event.target.name]: value
    });
    this.setState({
      lvl: this.state.exp/100
    })
  }

  handleSubmit(event) {
    alert('username: ' + this.state.username + '\nemail: ' + this.state.email + '\npassword: ' + this.state.password + '\nexp: ' + this.state.exp + '\nlvl: ' + this.state.lvl);
    event.preventDefault();
  }
  
  render() {
    return (
      <Fragment>
        <h1>Create Player</h1>
        <form onSubmit={this.handleSubmit}>
            <input type="text" value={this.state.username} name="username" onChange={this.handleChange} placeholder="USERNAME" />
          <br/>
            <input type="text" value={this.state.email} name="email" onChange={this.handleChange} placeholder="example@gmail.com" />
          <br/>
            <input type="password" value={this.state.password} name="password" onChange={this.handleChange} placeholder="******" />
          <br/>
            <input type="hidden" value={this.state.exp} name="exp" onChange={this.handleChange} placeholder="20" />
          <br/>
            {/* {console.log(this.state)} */}
          <Button
            className="btns"
            type="submit"
            buttonStyle="btn--outline"
            buttonSize="btn--large"
            data={this.state}
          >
            Submit
          </Button>
        </form>
      </Fragment>
    )
  }
}
