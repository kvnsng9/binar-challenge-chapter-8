import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './components/Navbar'
import Home from './components/pages/Home'
import UrAccount from './components/Player/DisplayAccount/UrAccount';
import EditAccount from './components/Player/EditAccount/EditAccount';
import CreatePlayer from './components/Player/CreatePlayer/CreatePlayer';

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar/>
        <header className="App-header">
          <Switch>
            <Route path='/' exact component={Home}/>
            <Route path='/create' component={CreatePlayer}/>
            <Route path='/edit' component={EditAccount}/>
            <Route path='/itsyouraccountbro/:data' component={UrAccount} />
          </Switch>
        </header>
      </Router>
    </div>
  );
}

export default App;
